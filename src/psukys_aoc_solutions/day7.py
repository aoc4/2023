from collections import Counter
from dataclasses import dataclass
from functools import cmp_to_key

CARD_ORDER = "23456789TJQKA"
JOKER_CARD_ORDER = "J23456789TQKA"


@dataclass
class Hand:
    cards: str
    bid: int


def adapted_joker_count(cards: str) -> list[int]:
    counter = Counter(cards)
    jokers = counter["J"]
    del counter["J"]
    commons = counter.most_common(2)
    if len(commons) > 1:
        return [commons[0][1] + jokers, commons[1][1]]
    elif len(commons) == 1:
        return [commons[0][1] + jokers]
    # only jokers
    return [jokers]

def compare_card_by_card(me: str, other: str) -> int:
    for my_card, other_card in zip(me, other):
        if my_card != other_card:
            return -1 if CARD_ORDER.index(my_card) < CARD_ORDER.index(other_card) else 1


def part1_cmp(self: Hand, other: Hand) -> int:
    self_counter = [count[1] for count in Counter(self.cards).most_common(2)]
    other_counter = [count[1] for count in Counter(other.cards).most_common(2)]
    if self_counter[0] == other_counter[0]:
        if len(self_counter) > 1:
            if self_counter[1] == other_counter[1]:
                return compare_card_by_card(self.cards, other.cards)    
            return -1 if self_counter[1] < other_counter[1] else 1
        return compare_card_by_card(self.cards, other.cards)
    return -1 if self_counter[0] < other_counter[0] else 1


def part2_cmp(self: Hand, other: Hand):
    self_counts = adapted_joker_count(self.cards)
    other_counts = adapted_joker_count(other.cards)
    if self_counts[0] == other_counts[0]:
        if len(self_counts) > 1:
            if self_counts[1] == other_counts[1]:
                return compare_card_by_card(self.cards, other.cards)
            return -1 if self_counts[1] < other_counts[1] else 1
        return compare_card_by_card(self.cards, other.cards)
    return -1 if self_counts[0] < other_counts[0] else 1


class Day7:
    def __init__(self, data: str):
        self.hands = []
        for line in data.splitlines():
            cards, bid = line.split()
            self.hands.append(Hand(cards, int(bid)))

    def solution1(self, cmp_func=part1_cmp):
        sorted_hands = sorted(self.hands, key=cmp_to_key(cmp_func))
        return sum((rank + 1) * hand.bid for rank, hand in enumerate(sorted_hands))

    def solution2(self):
        # TODO
        # return self.solution1(cmp_func=part2_cmp)
        return "Not solved"
