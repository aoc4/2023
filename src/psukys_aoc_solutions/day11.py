from itertools import combinations


class Day11:
    def __init__(self, data: str):
        self.data = data

    def parse_galaxies(self, data: list[str], multiplier: int = 2) -> list[tuple[int, int]]:
        # empty rows and cols get doubled - pad found galaxies
        empty_rows = [index for index, row in enumerate(data) if all("." == item for item in row)]
        empty_cols = [index for index in range(len(data[0])) if all("." == row[index] for row in data)]
        unpadded_galaxies = [(x, y) for y in range(len(data)) for x in range(len(data[0])) if data[y][x] == "#"]
        row_padded_galaxies = [
            (x, y + sum(multiplier - 1 for row in empty_rows if row < y))
            for x, y in unpadded_galaxies
        ]
        col_padded_galaxies = [
            (x + sum(multiplier - 1 for col in empty_cols if col < x), y)
            for x, y in row_padded_galaxies
        ]
        return col_padded_galaxies

    def get_distance(self, first: tuple[int, int], second: tuple[int, int]) -> int:
        return abs(first[0] - second[0]) + abs(first[1] - second[1])

    def solution1(self):
        galaxies = self.parse_galaxies(self.data.splitlines())
        return sum(self.get_distance(galaxy1, galaxy2) for galaxy1, galaxy2 in combinations(galaxies, 2))

    def solution2(self):
        galaxies = self.parse_galaxies(self.data.splitlines(), 1_000_000)
        return sum(self.get_distance(galaxy1, galaxy2) for galaxy1, galaxy2 in combinations(galaxies, 2))
