from dataclasses import dataclass


@dataclass
class Mapping:
    destination_start: int
    source_start: int
    range: int

    @property
    def offset(self):
        return self.destination_start - self.source_start


@dataclass
class Range:
    start: int
    end: int


class Day5:
    def __init__(self, data: str):
        packs = data.split("\n\n")
        seed_info, maps_info = packs[0], packs[1:]
        self.seeds = [int(seed) for seed in seed_info.split(": ")[1].split()]
        self.maps = {name: mappings for name, mappings in [self.parse_map(map_info) for map_info in maps_info]}


    def parse_map(self, map_info: str) -> tuple[str, list[Mapping]]:
        lines = map_info.split("\n")
        header = lines[0].split(" map")[0]
        mappings = []
        for line in lines[1:]:
            dest, source, offset = [int(item) for item in line.split()]
            mappings.append(Mapping(dest, source, offset))
        return header, mappings

    def get_destination(self, map: list[Mapping], source: int):
        for entry in map:
            diff = source - entry.source_start
            if 0 <= diff < entry.range:
                return source + entry.offset
        return source  # if not mapped, same destination number

    def solution1(self):
        chains = []
        for seed in self.seeds:
            mappings = {"seed": seed}
            latest_mapping = seed
            for name, map in self.maps.items():
                latest_mapping = self.get_destination(map, latest_mapping)
                mappings[name] = latest_mapping
            chains.append(mappings)
        locations = [idx["humidity-to-location"] for idx in chains]
        return min(locations)

    def get_range_destination(self, map: list[Mapping], seed_range: Range) -> list[Range]:
        mapped = []
        unmapped = [seed_range]
        for entry in map:
            entry_start = entry.source_start
            entry_end = entry_start + entry.range
            new_unmapped = []
            for seed in unmapped:
                # minimal requirements
                if entry_end >= seed.start and entry.source_start <= seed.end:
                    # out of mapping
                    if seed.start < entry.source_start:
                        new_unmapped.append(Range(seed.start, entry_start - 1))
                    if seed.end > entry_end:
                        new_unmapped.append(Range(entry_end + 1, seed.end))
                    # whatever's inside mapping - actually mapped
                    left = entry_start if entry_start >= seed.start else seed.start
                    right = entry_end if entry_end <= seed.end else seed.end
                    mapped.append(Range(left + entry.offset, right + entry.offset))  # shouldn't offset be -1'd?
                else:
                    new_unmapped.append(seed)
            unmapped = new_unmapped
        return mapped + unmapped
                

    def solution2(self):
        # try to just unpack and reuse solution1
        # nope, it just explodes haha
        # use ranges and BFS?
        # adapted_seeds = [Range(start, start + offset - 1) for start, offset in zip(self.seeds[:-1:2], self.seeds[1::2])]
        # chains = {"start": adapted_seeds}
        # for name, map in self.maps.items():
        #     new_seeds = []
        #     for seed in adapted_seeds:
        #         new_seeds.extend(self.get_range_destination(map, seed))
        #     chains[name] = new_seeds
        #     adapted_seeds = new_seeds
        # return min(new_seeds, key=lambda x: x.start).start

        # didn't work :(
        return "Not solved"