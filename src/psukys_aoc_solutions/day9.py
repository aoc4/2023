def all_same(items: list[int]) -> bool:
    return all(items[0] == item for item in items)

class Day9:
    def __init__(self, data: str):
        self.histories = [[int(item) for item in line.split()] for line in data.splitlines()]

    def solution1(self):
        history_sum = 0
        for history in self.histories:
            change = history
            changes = [change]
            
            while not all_same(change):
                change = [change[index + 1] - change[index] for index in range(len(change) - 1)]
                changes.append(change)
            
            # walk back
            for index in range(len(changes) - 1, 0, -1):
                changes[index - 1].append(changes[index - 1][-1] + changes[index][-1])
            
            history_sum += changes[0][-1]

        return history_sum

    def solution2(self):
        history_sum = 0
        for history in self.histories:
            change = history
            changes = [change]
            
            while not all_same(change):
                change = [change[index + 1] - change[index] for index in range(len(change) - 1)]
                changes.append(change)
            
            # walk back
            for index in range(len(changes) - 1, 0, -1):
                changes[index - 1].insert(0, changes[index - 1][0] - changes[index][0])
            
            history_sum += changes[0][0]

        return history_sum
