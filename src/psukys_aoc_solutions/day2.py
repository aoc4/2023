from dataclasses import dataclass, field
import re

RED_CAP = 12
GREEN_CAP = 13
BLUE_CAP = 14

@dataclass
class RevealedHand:
    red: int = 0
    blue: int = 0
    green: int = 0


@dataclass
class Game:
    identifier: int
    hands: list[RevealedHand]


class Day2:
    def __init__(self, data: str):
        self.games: list[Game] = [self.parse_game(line) for line in data.splitlines()]

    def parse_game(self, line: str) -> Game:
        match = re.match(r"Game (?P<id>\d+): (?P<draws>.+)", line)
        identifier = int(match.group("id"))
        draw_data = match.group("draws")
        draws = draw_data.split(";")
        hands = []
        for draw in draws:
            cubes = {}
            for cube in draw.split(","):
                number, color = cube.strip().split()
                cubes[color] = int(number)
            hands.append(RevealedHand(**cubes))
                    
        return Game(identifier=identifier, hands=hands)

    def solution1(self):
        possible_games =[
            game 
            for game in self.games 
            if all(hand.red <= RED_CAP and hand.green <= GREEN_CAP and hand.blue <= BLUE_CAP for hand in game.hands)
        ]
        return sum(game.identifier for game in possible_games)

    def solution2(self):
        # pre-built struct quite handy
        power = 0
        for game in self.games:
            max_red = 0
            max_green = 0
            max_blue = 0
            for hand in game.hands:
                max_red = max(max_red, hand.red)
                max_green = max(max_green, hand.green)
                max_blue = max(max_blue, hand.blue)
            power += max_red * max_green * max_blue
        return power