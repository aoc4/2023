NUMBER_MAPPINGS: dict[str, int] = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9"
}


class Day1:
    def __init__(self, data: str):
        self.data = data

    def get_calibration_value(self, line: str) -> int:
        # not optimal as whole line is iterated twice
        first = next(char for char in line if char.isdigit())
        # reversed - search from other end
        last = next(char for char in line[::-1] if char.isdigit())
        return int(first + last)

    def solution1(self) -> int:
        return sum(self.get_calibration_value(line) for line in self.data.splitlines())


    def get_real_calibration_value(self, line: str) -> int:
        # input data has cheeky overlaps
        # for our case we only care about first match
        # "find" first instance index and deduce by lowest which to use
        mapping_indices: dict[str, list[int]] = {
            mapping: [
                idx for idx in range(len(line)) if line.startswith(mapping, idx)
            ]
            for mapping in NUMBER_MAPPINGS
        }

        # flatten to dict[int, str]
        index_number_pairs: dict[int, str] = {
            index: NUMBER_MAPPINGS[mapping]
            for mapping, indices in mapping_indices.items()
            for index in indices
        }
        
        # add normal numbers to index_number_pairs
        for index, char in enumerate(line):
            if char.isdigit():
                index_number_pairs[index] = char

        first_char = min(index_number_pairs.keys())
        last_char = max(index_number_pairs.keys())

        return int(index_number_pairs[first_char] + index_number_pairs[last_char])

    def solution2(self):
        return sum(self.get_real_calibration_value(line) for line in self.data.splitlines())
