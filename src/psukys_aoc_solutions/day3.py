class Day3:
    def __init__(self, data: str):
        self.data = data.splitlines()
        self.limit_x = len(self.data[0]) - 1
        self.limit_y = len(self.data) - 1

    def get_neighbors(self, x: int, y: int) -> set[tuple[int, int]]:
        negative_x = max(0, x - 1)
        negative_y = max(0, y - 1)
        positive_x = min(self.limit_x, x + 1)
        positive_y = min(self.limit_y, y + 1)
        neighbors = {
                (negative_x, negative_y),
                (x, negative_y),
                (positive_x, negative_y),
                (negative_x, y),
                # skip x,y as it's target point
                (positive_x, y),
                (negative_x, positive_y),
                (x, positive_y),
                (positive_x, positive_y)
        }
        return neighbors

    def find_number(self, x: int, y: int) -> tuple[int, int, int]:
        """Return number's start coordinates and number itself"""
        line = self.data[y]
        number_data = line[x]
        left = max(0, x - 1)
        right = min(self.limit_x, x + 1)
        check_left = check_right = True
        while check_left or check_right:
            if left >= 0 and line[left].isdigit():
                number_data = line[left] + number_data
                left -= 1
            else:
                check_left = False
            
            if right <= self.limit_x and line[right].isdigit():
                number_data = number_data + line[right]
                right += 1
            else:
                check_right = False

        return left + 1, y, int(number_data)

    def solution1(self):
        numbers = dict()  # dict because of neighbor search approach
        for y in range(len(self.data)):
            for x in range(len(self.data[0])):
                item = self.data[y][x]
                if item != "." and not item.isdigit():
                    neighbors = self.get_neighbors(x, y)
                    for n_x, n_y in neighbors: # neighbor search can get duplicates
                        # 123
                        # .*.
                        # ...
                        # would result in 3 neighbors
                        if self.data[n_y][n_x].isdigit():
                            start_x, start_y, number = self.find_number(n_x, n_y)
                            numbers[(start_x, start_y)] = number
        return sum(numbers.values())

    def solution2(self):
        gear_ratio_sum = 0
        for y in range(len(self.data)):
            for x in range(len(self.data[0])):
                item = self.data[y][x]
                numbers = dict()
                if item == "*":
                    neighbors = self.get_neighbors(x, y)
                    for n_x, n_y in neighbors:
                        if self.data[n_y][n_x].isdigit():
                            start_x, start_y, number = self.find_number(n_x, n_y)
                            numbers[(start_x, start_y)] = number
                if len(numbers) == 2:
                    numbers_values = list(numbers.values())
                    gear_ratio_sum += numbers_values[0] * numbers_values[1]
        return gear_ratio_sum
