import math
import itertools

class Day8:
    def __init__(self, data: str):
        self.instructions, node_data = data.split("\n\n")
        self.nodes = {}
        for line in node_data.splitlines():
            node, links = line.split(" = ")
            left, right = links[1:-1].split(", ")
            self.nodes[node] = [left, right]

    def solution1(self):
        current = "AAA"
        steps = 0
        while current != "ZZZ":
            instruction = 0 if self.instructions[steps % len(self.instructions)] == "L" else 1
            current = self.nodes[current][instruction]
            steps += 1
        return steps

    def solution2(self):
        # works for test but not for input
        starters = [node for node in self.nodes.keys() if node.endswith("A")]
        # lcm
        starter_zs = []
        for node in starters:
            visited = []
            zs = []
            current = node
            steps = 0
            # identify looping
            while not current.endswith("Z"):
                visited.append(current)
                instruction = 0 if self.instructions[steps % len(self.instructions)] == "L" else 1
                current = self.nodes[current][instruction]
                steps += 1
                if current.endswith("Z"):
                    zs.append(steps)
            starter_zs.append(zs)

        lcms = [math.lcm(*step_combination) for step_combination in itertools.product(*starter_zs)]
        return min(lcms)



        while not all([node.endswith("Z") for node in current]):
            # identify loop
            # find where Zs match
            # find common denominator
            instruction = 0 if self.instructions[steps % len(self.instructions)] == "L" else 1
            current = [self.nodes[node][instruction] for node in current]
            steps += 1
        return steps
