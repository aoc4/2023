from dataclasses import dataclass


@dataclass
class Card:
    winning: list[str]
    my: list[str]

class Day4:
    def __init__(self, data: str):
        self.cards = self.parse_cards(data.splitlines())

    def parse_cards(self, lines: list[str]) -> list[Card]:
        cards = []

        for line in lines:
            numbers = line.split(":")[1]
            winning, my = numbers.split("|")
            winning = [num for num in winning.strip().split()]
            my = [num for num in my.strip().split()]
            cards.append(Card(winning, my))

        return cards

    def get_matches(self, cards: list[Card]) -> list[int]:
        return [sum(1 for num in card.my if num in card.winning) for card in cards]

    def solution1(self):
        matches = self.get_matches(self.cards)
        return sum([2 ** (match - 1) for match in matches if match > 0])

    def solution2(self):
        matches = self.get_matches(self.cards)
        scratchcards = [1 for _ in range(len(matches))]
        for index, match in enumerate(matches):
            start = index + 1
            end = start + match
            for step in range(start, end):
                scratchcards[step] += scratchcards[index]  # when reached, it will have all of the multiplicatives
        return sum(scratchcards)
