import networkx as nx


MAPPINGS = {
    "|": [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 0]
    ],
    "-": [
        [0, 0, 0],
        [1, 1, 1],
        [0, 0, 0]
    ],
    "L": [
        [0, 1, 0],
        [0, 1, 1],
        [0, 0, 0]
    ],
    "J": [
        [0, 1, 0],
        [1, 1, 0],
        [0, 0, 0]
    ],
    "7": [
        [0, 0, 0],
        [1, 1, 0],
        [0, 1, 0]
    ],
    "F": [
        [0, 0, 0],
        [0, 1, 1],
        [0, 1, 0]
    ],
    ".": [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "S": [
        [0, 1, 0],
        [1, 1, 1],
        [0, 1, 0]
    ]
}


class Day10:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def pad_map(self, unpadded: list[str]) -> list[str]:
        padded_line = "." * (len(unpadded[0]) + 2)
        return [padded_line] + [
            f".{line}." for line in unpadded
        ] + [padded_line]

    def get_neighbors(self, x: int, y: int, map: list[str]) -> list[tuple[int, int]]:
        mapping = MAPPINGS[map[y][x]]
        neighbors = []
        if mapping[1][0] == 1:  # check left
            neighbor = MAPPINGS[map[y][x - 1]]
            if neighbor[1][2] == 1:  # right side
                neighbors.append((x - 1, y))
        if mapping[2][1] == 1:  # check bottom
            neighbor = MAPPINGS[map[y + 1][x]]
            if neighbor[0][1] == 1:  # top side
                neighbors.append((x, y + 1))
        if mapping[1][2] == 1:  # check right
            neighbor = MAPPINGS[map[y][x + 1]]
            if neighbor[1][0] == 1:  # left side
                neighbors.append((x + 1, y))
        if mapping[0][1] == 1:  # check top
            neighbor = MAPPINGS[map[y - 1][x]]
            if neighbor[2][1] == 1:  # bottom side
                neighbors.append((x, y - 1))
        return neighbors

    def solution1(self):
        padded_map = self.pad_map(self.data)
        s_coordinates = [
            (x, y)
            for y, line in enumerate(padded_map)
            for x, char in enumerate(line)
            if char == "S"
        ][0]
        to_visit = {s_coordinates}
        visited = set()
        # TODO: we just visit, but not build loop
        graph = nx.Graph()
        while len(to_visit) != 0 :
            new_visits = set()
            for x, y in to_visit:
                visited.add((x, y))
                neighbors = self.get_neighbors(x, y, padded_map)
                for n_x, n_y in neighbors:
                    if (n_x, n_y) not in visited:
                        new_visits.add((n_x, n_y))
                        graph.add_edge((x, y), (n_x, n_y))
            to_visit = new_visits
        cycles = nx.simple_cycles(graph)
        biggest_cycle = sorted(cycles, key=lambda x: len(x))[-1]
        return len(biggest_cycle) // 2



    def solution2(self):
        ...
