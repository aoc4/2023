import functools
import operator

class Day6:
    def __init__(self, data: str):
        self.data = data
        

    def parse_part1_data(self, data: str) -> list[tuple[int, int]]:
        time_data, distance_data = data.splitlines()
        times = [int(time) for time in time_data.split(":")[1].strip().split()]
        distances = [int(dist) for dist in distance_data.split(":")[1].strip().split()]
        return list(zip(times, distances))

    def parse_part2_data(self, data: str) -> tuple[int, int]:
        time_data, distance_data = data.splitlines()
        time = int(time_data.split(":")[1].strip().replace(" ", ""))
        distance = int(distance_data.split(":")[1].strip().replace(" ", ""))
        return time, distance

    def get_charge_options(self, time: int, distance: int) -> list[int]:
        # optimize - only look from sides
        start = -1
        for time_step in range(time + 1):
            time_left = time - time_step
            speed = time_step
            if speed * time_left > distance:
                start = time_step
                break

        end = -1
        for time_step in range(time, 0, -1):
            time_left = time - time_step
            speed = time_step
            if speed * time_left > distance:
                end = time_step
                break
        return range(start, end + 1)

    def solution1(self):
        races = self.parse_part1_data(self.data)
        race_charge_options: list[list[int]] = [self.get_charge_options(time, distance) for time, distance in races]
        return functools.reduce(operator.mul, [len(option) for option in race_charge_options])

    def solution2(self):
        time, distance = self.parse_part2_data(self.data)
        charge_options = self.get_charge_options(time, distance)
        return len(charge_options)