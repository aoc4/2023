import pytest

from psukys_aoc_solutions.day3 import Day3


def test_1():
    data = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""
    expected = 4361
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""
    expected = 467835
    sut = Day3(data=data)
    assert expected == sut.solution2()
