import pytest

from psukys_aoc_solutions.day7 import Day7


def test_1():
    data = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"""
    expected = 6440
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"""
    expected = 5905
    sut = Day7(data=data)
    assert expected == sut.solution2()
