import pytest

from psukys_aoc_solutions.day9 import Day9


def test_1():
    data = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"""
    expected = 114
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"""
    expected = 2
    sut = Day9(data=data)
    assert expected == sut.solution2()
