import pytest

from psukys_aoc_solutions.day8 import Day8


def test_1_1():
    data = """RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)"""
    expected = 2
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_1_2():
    data = """LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)"""
    expected = 6
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"""
    expected = 6
    sut = Day8(data=data)
    assert expected == sut.solution2()
