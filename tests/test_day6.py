import pytest

from psukys_aoc_solutions.day6 import Day6


def test_1():
    data = """Time:      7  15   30
Distance:  9  40  200"""
    expected = 288
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """Time:      7  15   30
Distance:  9  40  200"""
    expected = 71503
    sut = Day6(data=data)
    assert expected == sut.solution2()
