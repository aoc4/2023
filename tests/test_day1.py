import pytest

from psukys_aoc_solutions.day1 import Day1


def test_1():
    data = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""
    expected = 142
    sut = Day1(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""
    expected = 281
    sut = Day1(data=data)
    assert expected == sut.solution2()


def test_overlapping_input():
    data = """oneightwoneight
eighthree
sevenine"""
    expected = 18 + 83 + 79
    sut = Day1(data=data)
    assert expected == sut.solution2()
